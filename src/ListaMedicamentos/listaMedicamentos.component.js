import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  Picker,
  Alert,
  TouchableHighlight,
  BackHandler
} from 'react-native';

import {
  ThemeProvider,
  Toolbar,
  Icon,
  BottomNavigation,
  Drawer,
  Avatar,
  Button,
  ListItem,
  ActionButton
} from 'react-native-material-ui';

import { Actions } from 'react-native-router-flux';
import { TextField } from 'react-native-material-textfield';

import { styles } from './styles/styles';

const matBase = {
  key: "Ingeniería de Software Iii",
  id: 100,
  codigo: "IS1052",
  nombreMateria: 'Ingeniería de Software Iii',
  cantidadTareas: 0,
  tipo:"group",
  tareas : []
};

export default class ListaMedicamentosView extends Component<{}> {
  constructor(props) {
    super(props);
    let {
      medicamentos,
      others
    }= props;

    this.state = {
      medicamentos,
      others,
      buscaMateria: '',
      searching: ''
    };

    this.searchRef = this.updateRef.bind(this, 'search');
    this.onPressSearch = this.onPressSearch.bind(this);
    this.onPressLogout = this.onPressLogout.bind(this);
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  onPressLogout() {
    this.search.blur();
    Alert.alert(
      'Cerrar sesión',
      '¿Deseas cerrar sesión?',
      [
        {text: 'ACEPTAR', onPress: () => Actions.loginView()},
        {text: 'CANCELAR', onPress: () => console.log('Cancel Pressed')},
      ],
      { cancelable: true }
    )
  };

  onPressSearch() {
    let { medicamentos, searching, others } = this.state;

    if (searching === '') {
      Alert.alert(
        'Código no ingresado',
        'Ingresa el código a buscar',
        [
          {text: 'Aceptar', onPress: () => console.log('Cancel Pressed')},
        ],
        { cancelable: true }
      );
      return;
    }

    var listaBusqueda = this.state.others,
      materiaBuscada = null;

    for (var indexMateria = 0; indexMateria < listaBusqueda.length; indexMateria++) {
      materia = listaBusqueda[indexMateria];
      if ((materia.tipo === "group") && (materia.codigo === this.state.searching)) {
        materiaBuscada = materia;
        break;
      }
    }

    if (materiaBuscada) {
      medicamentos.splice(0, 0, materiaBuscada);
    } else {
      Alert.alert(
        'Código no encontrado',
        'Intente ingresar otro código a buscar',
        [
          {text: 'Aceptar', onPress: () => console.log('Cancel Pressed')},
        ],
        { cancelable: true }
      );
      return;
    }

    this.setState({ medicamentos });

    this.search.blur();
  };

  borrarMateria = id => {
    var tempMed = [];

    for (var index = 0; index < this.state.medicamentos.length; index++) {
      materia = this.state.medicamentos[index];
      if (materia.id !== id) {
        tempMed.push(materia);
      }
    }

    this.setState({ medicamentos: tempMed });
  }

  onPressBorrarMateria = (nombreMateria, idItem) => {
    this.search.blur();
    Alert.alert(
      'Borrar materia',
      '¿Deseas borrar la materia '+ nombreMateria +'?',
      [
        {text: 'ACEPTAR', onPress: () => this.borrarMateria(idItem)},
        {text: 'CANCELAR', onPress: () => console.log('Cancel Pressed')},
      ],
      { cancelable: true }
    )
  };

  onPressMateria = (id) => {
    this.search.blur();
    var materia = null;
    for (var index = 0; index < this.state.medicamentos.length; index++) {
      temp = this.state.medicamentos[index];
      if (temp.id === id) {
        materia = temp;
        break;
      }
    }
    state = this.state;
    state.materiaEscogida = materia;
    Actions.listaTareasView(this.state)
  };

  onPressAgregarMateria() {
    this.search.blur();
    Actions.citaProgramadaView(this.state)
    //console.warn(this.state);
  };

  onChangeText = text => {
    this.setState({
      searching: text
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps='handled'
        >
          <View style={ styles.container }>
            <Toolbar
              style={{
                container:{ backgroundColor: '#ff7043'},
                centerElementContainer: {marginLeft: 0}
              }}
              centerElement={
                <Text style={ styles.logo }>MATERIAS</Text>
              }
              rightElement={
                <TouchableHighlight
                  onPress={this.onPressLogout}
                  underlayColor="#ff7043">
                  <Icon name="exit-to-app" color="#fff"/>
                </TouchableHighlight>
              }
            />

            <View style={styles.textInputWithIconContainer}>
              <View style={styles.textInputContainer}>
                <TextField
                  ref={this.searchRef}
                  keyboardType='email-address'
                  autoCapitalize='none'
                  autoCorrect={false}
                  enablesReturnKeyAutomatically={true}
                  onFocus={this.onFocus}
                  onChangeText={this.onChangeText}
                  onSubmitEditing={this.onSubmitEmail}
                  label='Busca una materia'
                  returnKeyType='next'
                  maxLength={50}
                  baseColor='white'
                  tintColor='white'
                  textColor='white'
                  highlightColor='white' />
              </View>
              <View style={styles.iconContainer}>
                <TouchableHighlight
                  onPress={this.onPressSearch}
                  underlayColor="#ff7043">
                  <Icon
                    name="search"
                    size={30}
                    color={"white"}
                    resizeMode='contain'
                  />
                </TouchableHighlight>
              </View>
            </View>

            <View style={ styles.sectionPagos }>
              {
                this.state.medicamentos.map(item => {
                  return (
                    <ListItem
                      divider
                      key={item.id}
                      value={item.nombreMateria}
                      leftElement={item.tipo}
                      centerElement={{
                        primaryText: item.nombreMateria,
                        secondaryText: item.cantidadTareas + ' tareas pendientes',
                        tertiaryText: 'Codigo: ' + item.codigo
                      }}
                      rightElement="delete"
                      onRightElementPress={
                          () => this.onPressBorrarMateria(item.nombreMateria, item.id)
                      }
                      onPress={
                        () => this.onPressMateria(item.id)
                      }

                    />
                  );
                })
              }

            </View>
          </View>
        </ScrollView>
        <ActionButton
          icon="add"
          onPress={
            () => this.onPressAgregarMateria()
          }
        />
      </View>
    );
  }
}
