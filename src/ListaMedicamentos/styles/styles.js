import {
  StyleSheet
} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  secLogo: {
    flex:8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginRight: 25
  },

  logoTitle: {
    flex: 8,
    alignItems: 'center',
    flexDirection: 'row'
  },

  logo: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 13,
  },

  barraInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7043',
    height: 35,
  },

  containerForm: {
    flex: 1,
  },

  textInputWithIconContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ff7043'
  },

  iconContainer: {
    width: 40,
    height: 30,
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
  },

  textInputContainer: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: 15,
    marginBottom: 10
  },

  textoBarraInfo: {
    color: '#fff',
    fontSize: 13,
  },

  profilePreview: {
    backgroundColor: '#0287D0',
    flex: 3,
    padding: 10,
    flexDirection: 'column',
  },

  imageProfileView: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    alignSelf: 'stretch'
  },

  fieldContainer: {
    flex: 1,
    alignSelf: 'stretch', 
    justifyContent: 'space-around',
  },

  sectionPagos: {
    flex: 8,
    backgroundColor: 'white'
  },

});
