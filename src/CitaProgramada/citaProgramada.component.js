import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  TouchableHighlight,
  ScrollView,
  Dimensions,
  BackHandler
} from 'react-native';

import {
  COLOR,
  ThemeProvider,
  Toolbar,
  Icon,
  Checkbox,
  BottomNavigation,
  Drawer,
  Avatar,
  Button,
  Subheader
} from 'react-native-material-ui';

import { styles } from './styles/styles';

import { Actions } from 'react-native-router-flux';

import { TextField } from 'react-native-material-textfield';

export default class CitaProgramadaView extends Component<{}> {
  constructor(props) {
    super(props);

    var materias = props.navigation.state.params.medicamentos;
    var others = props.navigation.state.params.others;

    this.state = {
      materias,
      others,
      materia: '',
      codigo: '',
      prioridad: '',
      group: false,
      sensei: 'miguel',
      base: {
        key: "Emprendimiento",
        id: 1,
        nombreMateria: 'Emprendimiento',
        cantidadTareas: 1,
        tipo:"group"
      },
      campoDeshabilitado: true,
      campoMateria: this.updateRef.bind(this, 'materia'),
      campoCodGrupo: this.updateRef.bind(this, 'grupo'),
      campoNombreSensei: this.updateRef.bind(this, 'sensei'),
    };

    this.onPressVolverAtras = this.onPressVolverAtras.bind(this);
    this._onPressedButton = this._onPressedButton.bind(this);

    BackHandler.addEventListener('hardwareBackPress', () => { this.onPressVolverAtras(); return true; });
  }

  onPressVolverAtras() {
    let medicamentos = this.state.materias;
    var state = { medicamentos, others: this.state.others };

    Actions.listaMedicamentosView(state);
  }

  _onPressedButton = () => {
    let {
      materia,
      codigo,
      group,
      base
    } = this.state;

    base.tipo = group ? "group" : "person";
    base.id = materia;
    base.key = materia;
    base.nombreMateria = materia;
    base.cantidadTareas = 0;
    base.codigo = group ? codigo : "No tiene";
    base.tareas = [];

    materias = this.state.materias;
    materias.push(base);


    let medicamentos = materias;
    var state = { medicamentos, others: this.state.others };

    Actions.listaMedicamentosView(state);
  }

  updateRef(name, ref) {
    this[name] = ref;
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{
            height: Dimensions.get('window').height - 50
          }}
        >
        <View style={ styles.container }>
          <Toolbar
            style={{
              container:{ backgroundColor: '#ff7043'},
              centerElementContainer: {marginLeft: 0}
            }}
            centerElement={
              <Text style={ styles.logo }>AGREGAR MATERIA</Text>
            }
            leftElement={
              <TouchableHighlight
                onPress={this.onPressVolverAtras}
                underlayColor="#ff7043">
                <Icon name="arrow-back" color="#fff"/>
              </TouchableHighlight>
            }
          />

          <View style={{flex:1}}>
            <View style={ styles.menuSection }>
              <TextField
                ref={this.state.campoMateria}
                label='Nombre de la materia'
                value={this.state.materia}
                onChangeText={ materia => this.setState({ materia }) }
              />
              <View style={{height: 20, marginTop:10}}>
                <Checkbox
                  label="Materia en grupo"
                  value="agree"
                  checked={this.state.group}
                  onCheck={group = () => {
                      if(this.state.campoDeshabilitado){
                        this.setState({campoDeshabilitado:false, group:true})
                      } else {
                        this.setState({campoDeshabilitado:true, group:false})
                      }
                    }
                  }
                />
              </View>
              <TextField
                label='Código del grupo'
                ref={this.state.campoCodGrupo}
                value={ this.state.codigo }
                onChangeText={ (codigo) => this.setState({ codigo }) }
                disabled={this.state.campoDeshabilitado}
              />
              <TextField
                label='Nombre del sensei'
                ref={this.state.campoNombreSensei}
                value={ this.state.sensei }
                onChangeText={ (sensei) => this.setState({ sensei }) }
                disabled={this.state.campoDeshabilitado}
              />
            </View>
            <View style={ styles.closeSectionButton}>
              <Button
                raised
                style={{
                  container: { height: 60, backgroundColor: '#ff7043'},
                  text:{ color: '#fff', fontSize: 16 }
                }}
                text="AGREGAR MATERIA"
                onPress={this._onPressedButton} />
            </View>
          </View>
        </View>
      </ScrollView>
      </View>
    );
  }
}
