import {
  StyleSheet
} from 'react-native';

export const buttonLoginStyle = StyleSheet.create({
  container:{
    height: 60,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },
  text: {
    color: "#ff7043",
    fontSize: 16
  },
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ff7043",
  },

  containerLogo: {
    flex:1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  },

  logoLogin: {
    width: 150,
    alignSelf: 'center'
  },

  containerForm: {
    flex: 1,
  },

  textInputWithIconContainer: {
    flex: 1,
    flexDirection: 'row',
  },

  iconContainer: {
    width: 45,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10
  },

  textInputContainer: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingRight: 15,
    marginBottom: 10
  },

  textFieldEmail: {
    justifyContent: 'flex-end',
  },

  textFieldPassword: {
    justifyContent: 'flex-start',
  },

  containerButtonInfo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  buttonContainer: {
    flex: 7,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    paddingHorizontal: 10,
  },

  forgotPasswordContainer:{
    flex: 3,
    alignSelf: 'stretch',
    flexDirection: 'row-reverse',
    marginRight: 20,
  },

  forgotPasswordText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginRight: 20,
  },


  secondContainerSignUp: {
    flex: 4,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10
  },

  textInfoSignup: {
    fontSize: 18,
    color: 'white'
  },

  signupButtonText: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold'
  },

  containerSignupInfo: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },

});
