import React, { Component } from 'react';

import {
  Text,
  View,
  Alert,
  ScrollView,
  Dimensions,
  BackHandler
} from 'react-native';

import {
  ThemeProvider,
  Button,
  Icon
} from 'react-native-material-ui';

import {
  styles,
  buttonLoginStyle
} from './styles/styles';

import validateEmail from './utils.js';

import { Actions } from 'react-native-router-flux';

import { TextField } from 'react-native-material-textfield';

import moment from 'moment';

var v25 = moment().utcOffset(0);
v25.set({hour:0,minute:0,second:25,millisecond:0});
v25.toISOString();
v25.format();

var v0 = moment().utcOffset(0);
v0.set({hour:0,minute:0,second:0,millisecond:0});
v0.toISOString();
v0.format();

var materiasUser1 = [
  {
    key: "Emprendimiento",
    id: 1,
    codigo: "ISC020",
    nombreMateria: 'Emprendimiento',
    cantidadTareas: 1,
    tipo:"group",
    tareas : [
      {
        key: "final",
        id: 1,
        nombreTarea: 'Trabajo final',
        prioridad: 'Alta',
        state: "done",
        timeContinue: 25,
        diasRestantes: '2 días restantes',
      },
    ]
  },
  {
    key: "APS",
    id: 2,
    codigo: "No tiene",
    nombreMateria: 'APS',
    cantidadTareas: 2,
    tipo:"person",
    tareas : [
      {
        key: "Desarrollo",
        id: 3,
        nombreTarea: 'Desarrollar App',
        prioridad: 'Alta',
        timeContinue: 0,
        state: "in_progress",
        diasRestantes: '5 días restantes',
      },
      {
        key: "Canvas",
        id: 2,
        nombreTarea: 'Desarrollar Canvas',
        prioridad: 'Media',
        timeContinue: 0,
        state: "urgent",
        diasRestantes: '0 días restantes',
      },
    ]
  },
];

var materiasUser2 = [
  {
    key: "Matemáticas IV",
    id: 5,
    codigo: "CB401",
    nombreMateria: 'Matemáticas IV',
    cantidadTareas: 0,
    tipo:"group",
    tareas : []
  },
  {
    key: "Microbiología",
    id: 9,
    codigo: "MB0001",
    nombreMateria: 'Microbiología',
    cantidadTareas: 1,
    tipo:"group",
    tareas : [
      {
        key: "Buscar mariposas",
        id: 10,
        nombreTarea: 'Buscar mariposas',
        prioridad: 'Baja',
        timeContinue: 0,
        state: "urgent",
        diasRestantes: '0 días restantes',
      },
    ]
  },
];

export default class LoginView extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      secureTextEntry: true,
      validate: true,
      emailUser1: "miguel@times.com",
      emailUser2: "christian@times.com",
      mat1: materiasUser1,
      mat2: materiasUser2
    };

    this.onFocus = this.onFocus.bind(this);
    this.onLogin= this.onLogin.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.generateAlert = this.generateAlert.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);
    this.onAccessoryPress = this.onAccessoryPress.bind(this);
    this.onPressSignUp = this.onPressSignUp.bind(this);

    this.emailRef = this.updateRef.bind(this, 'email');
    this.passwordRef = this.updateRef.bind(this, 'password');

    BackHandler.addEventListener('hardwareBackPress',
      this.onPressBack);
  };

  onPressBack() {
    return true;
  }

  onFocus() {
    let { errors = {} } = this.state;

    for (let name in errors) {
      let ref = this[name];

      if (ref && ref.isFocused()) {
        delete errors[name];
      }
    }

    this.setState({ errors });
  }

  onChangeText(text) {
    ['email', 'password']
      .map((name) => ({ name, ref: this[name] }))
      .forEach(({ name, ref }) => {
        if (ref.isFocused()) {
          this.setState({ [name]: text });
        }
      });
  }

  onAccessoryPress() {
    this.setState(({ secureTextEntry }) =>
      ({ secureTextEntry: !secureTextEntry }));
  }

  onSubmitEmail() {
    this.password.focus();
  }

  onSubmitPassword() {
    this.password.blur();
  }

  onSubmit() {
    this.email.blur();
    this.password.blur();
    let errors = {};
    this.validate = true;
    ['email', 'password']
      .forEach((name) => {
        let value = this[name].value();
        if (!value) {
          this.generateAlert(
            `Ningún campo puede estar vacío`,"",true);
          this.validate = false;
        } else if ('email' === name && !validateEmail(value)) {
          this.generateAlert(
            "Correo inválido","",true);
            this.validate = false;
        } else if ('password' === name && value.length < 6) {
          this.generateAlert(
            "Contraseña muy corta","",true);
          this.validate = false;
        }
      });
    this.onLogin();
  }

  onLogin= () => {
    if(this.validate == true){
      let dataState = this.state;
      let dataToSend = {
        email: dataState.email.toLowerCase(),
        password: dataState.password,
        medicamentos: []
      };
      if(dataToSend.email == dataState.emailUser1){
        dataToSend.medicamentos = materiasUser1;
        dataToSend.others = materiasUser2;
        Actions.listaMedicamentosView(dataToSend);
        //console.warn(dataToSend);
      }else if(dataToSend.email == dataState.emailUser2){
        dataToSend.medicamentos = materiasUser2;
        dataToSend.others = materiasUser1;
        Actions.listaMedicamentosView(dataToSend);
      }else{
        this.generateAlert(
          "Usuario incorrecto","",true);
      }

    }
  }


  updateRef(name, ref) {
    this[name] = ref;
  }


  onPressSignUp() {
    this.email.blur();
    this.password.blur();
    Actions.registro1View();
  }

  generateAlert = (title, message, cancelable) => {
    Alert.alert(
      title,
      message,
      [
        {text: 'Ok', style: 'cancel'},
      ],
      { cancelable }
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{
            height: Dimensions.get('window').height - 50
          }}
        >
          <View style={styles.container}>
            <View style={styles.containerLogo}>
              <Icon
                name="alarm-add"
                size={100}
                color={"white"}
                resizeMode='contain'
                />
              <Text
                style={styles.signupButtonText}>
                  TIMES UP
              </Text>
            </View>

            <View style={styles.containerForm}>
              <View style={styles.textInputWithIconContainer}>
                <View style={styles.iconContainer}>
                  <Icon
                    name="account-circle"
                    size={38}
                    color={"white"}
                    resizeMode='contain'
                    />
                </View>
                <View style={styles.textInputContainer}>
                  <TextField
                    ref={this.emailRef}
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    onFocus={this.onFocus}
                    onChangeText={this.onChangeText}
                    onSubmitEditing={this.onSubmitEmail}
                    label='Correo electrónico'
                    returnKeyType='next'
                    maxLength={50}
                    baseColor='white'
                    tintColor='white'
                    textColor='white'
                    highlightColor='white'/>
                </View>
              </View>
              <View style={styles.textInputWithIconContainer}>
                <View style={styles.iconContainer}>
                  <Icon
                    name="lock"
                    size={38}
                    color={"white"}
                    />
                </View>
                <View style={styles.textInputContainer}>
                  <TextField
                    ref={this.passwordRef}
                    autoCapitalize='none'
                    autoCorrect={false}
                    enablesReturnKeyAutomatically={true}
                    secureTextEntry={true}
                    onFocus={this.onFocus}
                    onChangeText={this.onChangeText}
                    onSubmitEditing={this.onSubmit}
                    returnKeyType='done'
                    label='Contraseña'
                    maxLength={50}
                    baseColor='white'
                    tintColor='white'
                    textColor='white'
                    highlightColor='white'/>
                </View>
              </View>
            </View>
            <View style={styles.containerButtonInfo}>
              <View style={styles.buttonContainer}>
                <Button
                  style={buttonLoginStyle}
                  onPress={this.onSubmit}
                  text="Ingresar" />
              </View>
            </View>
            <View style={styles.containerSignupInfo}>
              <View>
                <Text
                  style={styles.signupButtonText}
                  onPress={this.onPressSignUp}>
                    ¡Regístrate!
                </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
