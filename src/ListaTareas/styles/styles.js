import {
  StyleSheet
} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  secLogo: {
    flex:8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginRight: 25
  },

  logoTitle: {
    flex: 8,
    alignItems: 'center',
    flexDirection: 'row'
  },

  logo: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 13,
  },

  barraInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7043',
    height: 35,
  },

  textoBarraInfo: {
    color: '#fff',
    fontSize: 13,
  },

  profilePreview: {
    backgroundColor: '#0287D0',
    flex: 3,
    padding: 10,
    flexDirection: 'column',
  },

  imageProfileView: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    alignSelf: 'stretch'
  },

  fieldContainer: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'space-around',
  },

  sectionPagos: {
    flex: 8,
    backgroundColor: 'white'
  },

});
