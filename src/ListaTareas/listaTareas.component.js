import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions,
  Picker,
  Alert,
  TouchableHighlight,
  BackHandler
} from 'react-native';

import {
  ThemeProvider,
  Toolbar,
  Icon,
  BottomNavigation,
  Drawer,
  Avatar,
  Button,
  ListItem,
  ActionButton
} from 'react-native-material-ui';

import { styles } from './styles/styles';

import { Actions } from 'react-native-router-flux';

export default class ListaTareasView extends Component<{}> {

    constructor(props) {
      super(props);


      this.state = props.navigation.state.params;
      this.state.tareas = [];

      for (var index = 0; index < this.state.medicamentos.length; index++) {
        mat = this.state.medicamentos[index];
        if (mat === this.state.materiaEscogida) {
          this.state.tareas = mat.tareas;
          break;
        }
      }

      this.borrarTarea = this.borrarTarea.bind(this);
      this.onPressVolverAtrasLT = this.onPressVolverAtrasLT.bind(this);
      this.onPressBorrarTarea = this.onPressBorrarTarea.bind(this);
      this.onPressIrATarea = this.onPressIrATarea.bind(this);

      BackHandler.addEventListener('hardwareBackPress', () => { this.onPressVolverAtrasLT(); return true; });
    }

    borrarTarea(nombreTarea) {
      var tareas = [];
      for (var index = 0; index < this.state.tareas.length; index++) {
        var tarea = this.state.tareas[index];
        if (tarea.nombreTarea !== nombreTarea) {
          tareas.push(tarea);
        }
      }

      var medicamentos = this.state.medicamentos;

      for (var index = 0; index < medicamentos.length; index++) {
        mat = medicamentos[index];
        if (mat === this.state.materiaEscogida) {
          medicamentos[index].tareas = tareas;
          medicamentos[index].cantidadTareas = tareas.length;
          break;
        }
      }
      this.setState({ tareas, medicamentos });
    }

    onPressBorrarTarea = nombreTarea => {
      Alert.alert(
        'Borrar tarea',
        '¿Deseas borrar la tarea '+ nombreTarea +'?',
        [
          {text: 'ACEPTAR', onPress: () => {
            this.borrarTarea(nombreTarea);
          }},
          {text: 'CANCELAR', onPress: () => console.warn('Cancel Pressed')},
        ],
        { cancelable: true }
      )
    }

    onPressVolverAtrasLT() {
      var medicamentos = this.state.medicamentos;
      var others = this.state.others;
      let state = { medicamentos, others };

      Actions.listaMedicamentosView(state);
    }

    onPressAgregarTarea = () => {
      Actions.agregarTareaView({state: this.state});
    }

    onPressIrATarea = item => {
        let { nombreTarea, prioridad, diasRestantes, timeContinue } = item;
        let { medicamentos, materiaEscogida, others } = this.state;

        medicamentos.nombreTarea = nombreTarea;
        medicamentos.prioridad = prioridad;
        medicamentos.diasRestantes = diasRestantes;
        medicamentos.timeContinue = timeContinue;
        medicamentos.tarea = item;

        let state = { medicamentos, materiaEscogida, others };

        Actions.verTareaView(state);
    }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps='handled'
        >
        <View style={ styles.container }>
          <Toolbar
            style={{
              container:{ backgroundColor: '#ff7043'},
              centerElementContainer: {marginLeft: 0}
            }}
            centerElement={
              <Text style={ styles.logo }>TAREAS</Text>
            }
            leftElement={
              <TouchableHighlight
                onPress={this.onPressVolverAtrasLT}
                underlayColor="#ff7043">
                <Icon name="arrow-back" color="#fff"/>
              </TouchableHighlight>
            }
            rightElement={<Icon name="arrow-back" color="#ff7043"/>}
          />
          <View style={ styles.sectionPagos }>
            {
              this.state.tareas.map(item => {
                stateItem = item.state === "urgent" ? "warning" : (item.state === "in_progress" ? "insert-drive-file" : item.state);
                return (
                  <ListItem
                    divider
                    key={item.id}
                    leftElement={stateItem}
                    centerElement={{
                      primaryText: item.nombreTarea,
                      secondaryText: item.prioridad,
                      tertiaryText: item.diasRestantes,
                    }}
                    rightElement="delete"
                    onPress={() => this.onPressIrATarea(item)}
                    onRightElementPress={
                      () => this.onPressBorrarTarea(item.nombreTarea)
                    }
                  />
                );
              })
            }
          </View>
        </View>
        </ScrollView>
        <ActionButton
          icon="add"
          onPress={
            () => this.onPressAgregarTarea()
          }
        />
        </View>
    );
  }
}
