import {
  StyleSheet
} from 'react-native';

export const buttonStyle = StyleSheet.create({
  container:{
    height: 60,
    alignSelf: 'stretch',
    backgroundColor: '#ff7043'
  },

  text: {
    color: 'white',
    fontSize: 16
  }
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

   secLogo: {
    flex:8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginRight: 30
  },

  logoTitle: {
    flex: 8,
    alignItems: 'center',
    flexDirection: 'row'
  },

  logo: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 13,
  },

  barraInfo: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7043'
  },

  textoBarraInfo: {
    color: '#fff',
    fontSize: 13
  },

  contentContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  messageContainer: {
    flex: 2,
    alignSelf: 'stretch',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },

  welcomeText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ff7043',
    marginVertical: 10
  },

  normalText: {
    fontSize: 16,
    color: 'gray',
    textAlign: 'center',
    marginHorizontal: 20,
  },

  fieldsContainer: {
    flex: 4,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    marginHorizontal: 20,
    marginBottom: 10
  },

  buttonContainer: {
    flex: 2,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    margin: 20,
  },
  
    header: {
    borderBottomWidth: 0.5,
    paddingTop: 20,
    paddingBottom: 10,
    backgroundColor: '#f9f9f9'
  },

  title: {
    alignSelf: 'center',
    fontWeight: '600',
  },


  timerWrapper: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    flex:2,
  },

  top: {
    flex:1,
  },

  bottom: {
    flex:2,
    backgroundColor: 'white'
  },

  timerWrapperInner: {
    alignSelf: 'center',
  },

  mainTimer: {
    fontSize: 60,
    fontWeight: '100',
    alignSelf: 'flex-end'
  },

  lapTimer: {
    fontSize: 18,
    alignSelf: 'center',
    marginBottom: 5,
  },

  lapRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 40,
    paddingTop: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: '#ddd'
  },

  lapNumber: {
    fontSize: 16,
    color: '#777',
  },

  lapTime: {
    color: '#000',
    fontSize: 20,
    fontWeight: '300'
  },

  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex:1
  },

  button: {
    height: 40,
    width: 40,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },

  medicoEspecialista: {
    paddingTop:10,
    paddingBottom: 30,
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center',
    borderWidth: 0.5
  },

  textoTipoMedico: {
    color: '#757575',
    fontSize: 20,
    marginBottom: 10,
  },

  textoInfo: {
    color: '#a0a0a0',
    fontSize: 30,
  },

  renglonTexto: {
    alignContent: 'center',
    alignItems: 'center',
  }
});
