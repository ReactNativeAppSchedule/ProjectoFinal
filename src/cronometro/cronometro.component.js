import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ListView,
  BackHandler
} from 'react-native';

import {
  ThemeProvider,
  Toolbar,
  Icon,
  Button
} from 'react-native-material-ui';

import TimeFormatter from 'minutes-seconds-milliseconds';
import { Actions } from 'react-native-router-flux';

import moment from 'moment';

let ds = new ListView.DataSource({
  rowHasChanged: (row1, row2) => row1 !== row2,
});

import { styles } from './styles/styles';

var cero = moment().utcOffset(0);

export default class VerTareaView extends Component<Props> {
  constructor(props){
    super(props);

    let{
      medicamentos,
      materiaEscogida,
      others
    } = props.navigation.state.params;

    var time = medicamentos.timeContinue !== 0 ? medicamentos.timeContinue : 0;
    cero.set({hour:0,minute:0,second:time,millisecond:0});
    cero.toISOString();
    cero.format();

    this.state = {
      time,
      others,
      medicamentos,
      materiaEscogida,
      isRunning: false,
      mainTimer: cero,
      lapTimer: null,
      mainTimerStart: "00:00:00",
      lapTimerStart: null,
      interval: 1
    }

    this.onPressVolverAtras = this.onPressVolverAtras.bind(this);
    this.handleStartStop = this.handleStartStop.bind(this);
    BackHandler.addEventListener(
      'hardwareBackPress',
      () => {this.onPressVolverAtras(false);  return true;});
  }

  _renderTimers(){
    return(
      <View style={styles.timerWrapper}>
        <View style={styles.timerWrapperInner}>
          <Text style={styles.lapTimer}>Tiempo de actividad</Text>
          <Text style={styles.mainTimer}>00:{this.state.mainTimer.format("mm:ss")}</Text>
        </View>
      </View>
    );
  }

  onPressVolverAtras(done) {
    clearInterval(this.state.interval);

    if (this.state.isRunning) this.handleStartStop();

    clearInterval(this.state.interval);

    var medicamentos = this.state.medicamentos;
    medicamentos.tarea.timeContinue = this.state.time;

    if (done) { medicamentos.tarea.state = "done"; }

    this.setState({
      medicamentos
    });

    for (var i = 0; i < this.state.medicamentos.length; i++) {
      materia = this.state.medicamentos[i];

      if (materia.key === this.state.materiaEscogida.key) {
        for (var indexTareas = 0; indexTareas < materia.tareas.length; indexTareas++) {
          tarea = materia.tareas[indexTareas];

          if (tarea.key === this.state.medicamentos.tarea.key) {
            materia.tareas[indexTareas] = this.state.medicamentos.tarea;
            break;
          }
        }
        medicamentos = this.state.medicamentos;
        medicamentos[i] = materia;
        this.setState({
          medicamentos
        });
        break;
      }
    }

    clearInterval(this.state.interval);
    Actions.listaTareasView(this.state);
    return true;
  }

  handleStartStop(){
    let { isRunning, firstTime, mainTimer, lapTimer } = this.state;

    this.setState({
      mainTimer: cero
    });

    clearInterval(this.state.interval);

    if(isRunning){
      this.setState({
        isRunning: false
      });
    } else {
      cero.set({hour:0,minute:0,second:this.state.time,millisecond:0});
      cero.toISOString();
      cero.format();

      this.setState({
        mainTimerStart: new Date(),
        lapTimerStart: new Date(),
        isRunning: true,
        mainTimer: cero
      });

      var int = setInterval(() => {
        if (this.state.isRunning) {
          mainTimer = mainTimer.add(1, 'second');
          this.setState({
            mainTimer,
            time:this.state.time+1
          });
        }
      }, 1000);

      this.setState({
        interval: int
      });
    }

  }

  handleLapReset() {
    let {isRunning, mainTimerStart} = this.state;

    if(mainTimerStart && !isRunning){
      this.setState({
        mainTimerStart: null,
        lapTimerStart: null,
        mainTimer: 0,
        lapTimer: 0,
      });
    }
  }

  _renderButtons(){
    return(
      <View style={styles.buttonWrapper}>
        <TouchableHighlight underlayColor='white' onPress={() => this.handleStartStop()} style={styles.button}>
          <Icon color="gray" size={40} name={this.state.isRunning ? "pause" : "play-arrow"}/>
        </TouchableHighlight>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          style={{
            container:{ backgroundColor: '#ff7043'},
            centerElementContainer: {marginLeft: 0}
          }}
          centerElement={
            <Text style={ styles.logo }>{this.state.medicamentos.nombreTarea}</Text>
          }
          leftElement={
            <TouchableHighlight
              onPress={() => this.onPressVolverAtras(false)}
              underlayColor="#ff7043">
              <Icon name="arrow-back" color="#fff"/>
            </TouchableHighlight>
          }
          rightElement={
            <TouchableHighlight
              onPress={() => this.onPressVolverAtras(true)}
              underlayColor="#ff7043">
              <Icon name="done" color="#fff"/>
            </TouchableHighlight>
          }
        />

          <View style={styles.top}>
            {this._renderTimers()}
            {this._renderButtons()}
          </View>
          <View style={styles.bottom}>
            <View
              style={ styles.medicoEspecialista }
              underlayColor="#fff"
              onPress={this.onPressService}>
              <View>
                <View style={styles.renglonTexto}>
                  <Text style={styles.textoTipoMedico}>Plazo de entrega</Text>
                  <Text style={ styles.textoInfo }>{this.state.medicamentos.diasRestantes}</Text>
                </View>
              </View>
            </View>
            <View
              style={ styles.medicoEspecialista }
              underlayColor="#fff"
              onPress={this.onPressEspecialista}>
              <View>
                <View style={styles.renglonTexto}>
                  <Text style={styles.textoTipoMedico}>Prioridad</Text>
                  <Text style={ styles.textoInfo }>{this.state.medicamentos.prioridad}</Text>
                </View>
              </View>
            </View>
            <View
              style={ styles.medicoEspecialista }
              underlayColor="#fff"
              onPress={this.onPressService}>
              <View>
                <View style={ styles.renglonTexto }>
                  <Text style={ styles.textoTipoMedico }>Tiempo trabajado</Text>
                  <Text style={ styles.textoInfo }>00:{this.state.mainTimer.format("mm:ss")}</Text>
                </View>
              </View>
            </View>
          </View>
      </View>
    );
  }
}
