import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  TouchableHighlight,
  ScrollView,
  Dimensions,
  BackHandler
} from 'react-native';

import {
  COLOR,
  ThemeProvider,
  Toolbar,
  Icon,
  RadioButton,
  BottomNavigation,
  Drawer,
  Avatar,
  Button,
  Subheader,
} from 'react-native-material-ui';

import { styles } from './styles/styles';
import { TextField } from 'react-native-material-textfield';
import DatePicker from 'react-native-datepicker';
import { Actions } from 'react-native-router-flux';

import moment from 'moment';

export default class AgregarTareaView extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {
      oldState: props.state,
      tarea: '',
      prioridad: '',
      date: moment().format("YYYY-MM-DD"),
      maxDate: moment().add(365, 'days').format('LLL'),
      prioridadSelected: -1,
      prioridad: [
        {key: "Alta", id:1},
        {key: "Media", id:2},
        {key: "Baja", id:3},
      ]
    };

    this.onPressVolverAtras = this.onPressVolverAtras.bind(this);

    this._crearTarea = this._crearTarea.bind(this);
    BackHandler.addEventListener('hardwareBackPress',
      () => {
        this.onPressVolverAtras();
        return true;
      });
  }

  updateRef(name, ref) {
    this[name] = ref;
  }

  onPressVolverAtras() {
    Actions.listaTareasView(this.state.oldState)
  }

  _crearTarea = () => {
    let { tarea, date, prioridadSelected } = this.state;

    if ((prioridadSelected === -1) || (tarea === '')) {
      Alert.alert(
        'Llenar campos',
        'Todos los campos son requeridos',
        [
          {text: 'OK', onPress: () => console.log('')},
        ],
        { cancelable: true }
      );
    } else {
      var prioridad, fecha;
      if (prioridadSelected === 1) prioridad = "Alta";
      else if (prioridadSelected === 2) prioridad = "Media";
      else if (prioridadSelected === 3) prioridad = "Baja";

      var processDate = new Date(date);
      var fecha = moment(processDate).diff(moment(new Date()), 'days') + 1;

      if (
        ((new Date()).getDate()-1 === processDate.getDate())
        && ((new Date()).getMonth() === processDate.getMonth())
      ) fecha = 0;

      tarea = {
        key: tarea,
        id: tarea,
        nombreTarea: tarea,
        prioridad: prioridad,
        state: "in_progress",
        diasRestantes: `${fecha} días restantes`,
      };

      let state = this.state.oldState;

      state.materiaEscogida.tareas.push(tarea);
      state.materiaEscogida.cantidadTareas = state.materiaEscogida.tareas.length;

      for (var index = 0; index < state.medicamentos.length; index++) {
        materia = state.medicamentos[index];
        if (materia.nombreMateria === state.materiaEscogida.nombreMateria) {
          state.medicamentos[index] = state.materiaEscogida;
          break;
        }
      }

      Actions.listaTareasView(state);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{
            height: Dimensions.get('window').height - 50
          }}
        >
          <View style={ styles.container }>
            <Toolbar
              style={{
                container: {backgroundColor: '#ff7043'},
                centerElementContainer: {marginLeft: 0}
              }}
              leftElement={
                <TouchableHighlight
                  onPress={this.onPressVolverAtras}
                  underlayColor="#ff7043">
                  <Icon name="arrow-back" color="#fff"/>
                </TouchableHighlight>
              }
              rightElement={<Icon name="done" color="#ff7043"/>}
              onPressRightElement={() => {this._crearTarea()}}

              centerElement={
                <Text style={ styles.logo }>AGREGAR TAREA</Text>
              }
            />
            <View style={{flex:1}}>
              <View style={ styles.timeSection }>
                <Text
                  style={ styles.textGray2 }>
                  Fecha límite de la tarea
                </Text>
                <DatePicker
                  style={{width: 300}}
                  date={this.state.date}
                  mode="date"
                  placeholder="Seleccione la fecha"
                  format="YYYY-MM-DD"
                  minDate={new Date()}
                  maxDate={this.state.maxDate}
                  is24Hour={false}
                  confirmBtnText="Confirmar"
                  cancelBtnText="Cancelar"
                  iconSource={require('../assets/CitaProgramada/dateIcon.png')}
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                      borderWidth: 1,
                      borderColor: 'lightgray',
                      marginLeft: 36,
                      borderRadius: 5,
                    },
                    dateText: {
                      fontSize: 18,
                      color: '#b0b0b0'
                    }
                  }}
                  onDateChange={(date) => {this.setState({ date })}}
                />
              </View>
              <View style={ styles.menuSection }>
                <TextField
                  label='Nombre de la tarea'
                  value={ this.tarea }
                  onChangeText={ (tarea) => this.setState({ tarea }) }
                />
                <Subheader text="Prioridad" />
                {
                  this.state.prioridad.map(item => {
                    return (
                      <View key={item.id} style={{height:20, marginVertical:7}}>
                        <RadioButton
                          key={item.id}
                          label={item.key}
                          value={item.id}
                          checked={this.state.prioridadSelected == item.id}
                          onSelect={checked => this.setState({ prioridadSelected: item.id })}
                        />
                      </View>
                    );
                  })
                }
              </View>
              <View style={ styles.closeSectionButton}>
                <Button
                  raised
                  style={{
                    container: { height: 60, backgroundColor: '#ff7043'},
                    text:{ color: '#fff', fontSize: 16 }
                  }}
                  text="Crear tarea"
                  onPress={this._crearTarea} />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
