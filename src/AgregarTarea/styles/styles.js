import{
	StyleSheet
} from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },

  secLogo: {
    flex:8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginRight: 30
  },

  logoTitle: {
    flex: 8,
    alignItems: 'center',
    flexDirection: 'row'
  },

  logo: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 13,
  },

  barraInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7043',
    height: 35,
  },

  timeSection: {
    backgroundColor: 'white',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: '#bdbdbd',
    paddingBottom: 20,
    paddingHorizontal: 10,
  },

  textGray2: {
    color: 'gray',
    fontSize: 16,
    marginVertical: 10,
    fontWeight: 'bold',
  },

  textoBarraInfo: {
    color: '#fff',
    fontSize: 13
  },

  menuSection: {
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 10,
  },

  closeSectionButton: {
    backgroundColor: 'white',
    padding: 10,
		marginBottom:10,
  }

});
