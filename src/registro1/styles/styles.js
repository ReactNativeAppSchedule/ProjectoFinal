import {
  StyleSheet
} from 'react-native';

export const buttonStyle = StyleSheet.create({
  container:{
    height: 60,
    alignSelf: 'stretch',
    backgroundColor: '#ff7043'
  },

  text: {
    color: 'white',
    fontSize: 16
  }
});

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white',
  },

   secLogo: {
    flex:8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginRight: 30
  },

  logoTitle: {
    flex: 8,
    alignItems: 'center',
    flexDirection: 'row'
  },

  logo: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 13,
  },

  barraInfo: {
    height: 35,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ff7043'
  },

  textoBarraInfo: {
    color: '#fff',
    fontSize: 13
  },

  contentContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  messageContainer: {
    flex: 2,
    alignSelf: 'stretch',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20
  },

  welcomeText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ff7043',
    marginVertical: 10
  },

  normalText: {
    fontSize: 16,
    color: 'gray',
    textAlign: 'center',
    marginHorizontal: 20,
  },

  fieldsContainer: {
    flex: 4,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    marginHorizontal: 20,
    marginBottom: 10
  },

  buttonContainer: {
    flex: 2,
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
    margin: 20,
  },
  
});
