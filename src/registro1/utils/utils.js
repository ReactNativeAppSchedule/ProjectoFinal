let validateEmail = email => {
	if ((!isNaN(parseInt(email[0]))) || (!isNaN(parseInt(email[email.length-1])))) {
		return false;
	}
	if (email.includes(" ")) {
		return false;
	}
	if (!email.includes("@")) {
		return false;
	}
	if (!email.split("@")[1].includes(".")) {
		return false;
	}
	if (!email.split("@")[1].length === 0) {
		return false;	
	}

	return true;
}

export default validateEmail;
