import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import { 
  Icon,
  Toolbar,
} from 'react-native-material-ui';

const ToolbarD = props => {
  const {
    iconColorLeft,
    iconColorRight,
    onPressLeft,
    onPressRight
  } = props;

  return (
    <Toolbar
      style={{
        centerElementContainer: {marginLeft: 0}
      }}
      leftElement={
        <TouchableHighlight
          onPress={onPressLeft}
          underlayColor={"#ff7043"}>
          <Icon name="arrow-back" color={iconColorLeft}/>
        </TouchableHighlight>
      }

      rightElement={
        <TouchableHighlight
          onPress={onPressRight}
          underlayColor={"#ff7043"}>
          <Icon name="done" color={iconColorRight}/>
        </TouchableHighlight>
      }
    />
  )
}

const styles = StyleSheet.create({
 logo: {
    width: 60,
    alignSelf: 'center',
  },
});

export default ToolbarD;