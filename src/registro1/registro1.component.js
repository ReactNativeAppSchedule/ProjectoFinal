import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  ScrollView,
  Dimensions,
  BackHandler,
  TouchableHighlight,
  NetInfo
} from 'react-native';

import {
  ThemeProvider,
  Toolbar,
  Icon,
  Button
} from 'react-native-material-ui';

import { TextField } from 'react-native-material-textfield';

import ToolbarD from './utils/toolbar';

import { Actions } from 'react-native-router-flux';

import validateEmail from './utils/utils.js';

import {
  styles,
  buttonStyle
} from './styles/styles';

export default class Registro1View extends Component<{}> {
  constructor(props) {
    super(props);

    this.onFocus = this.onFocus.bind(this);
    this.onBackPressed = this.onBackPressed.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
    this.onSubmitName = this.onSubmitName.bind(this);
    this.onSubmitEmail = this.onSubmitEmail.bind(this);
    this.onSubmitPassword = this.onSubmitPassword.bind(this);

    this.nameRef = this.updateRef.bind(this, 'name');
    this.emailRef = this.updateRef.bind(this, 'email');
    this.PasswordRef = this.updateRef.bind(this, 'Password');

    this.state = {
      name: "",
      email: "",
      Password: "",
      validate: true,
    };

    BackHandler.addEventListener(
      'hardwareBackPress',
      this.onBackPressed
    );
  };

onFocus() {
  let { errors = {} } = this.state;

  for (let name in errors) {
    let ref = this[name];

    if (ref && ref.isFocused()) {
      delete errors[name];
    }
  }

  this.setState({ errors });
}

updateRef(name, ref) {
  this[name] = ref;
}

onSubmitName() {
  this.email.focus();
}

onSubmitEmail() {
  this.Password.focus();
}

onSubmitPassword() {
  this.Password.blur();
  this.generateAlert("Cuenta creada", "Ahora disfruta de la aplicaión", true);
  Actions.loginView();
}

onBackPressed = () => {
  Actions.loginView();
  return true;
}

generateAlert = (title, message, cancelable) => {
  Alert.alert(
    title,
    message,
    [
      {text: 'Ok', style: 'cancel'},
    ],
    { cancelable }
  );
}

changeLoading = (state) => {
  this.setState({
    loading: state

  });
}

onChangeText(text) {
  ['name', 'email', 'Password']
    .map((name) => ({ name, ref: this[name] }))
    .forEach(({ name, ref }) => {
      if (ref.isFocused()) {
        this.setState({ [name]: text });
      }
    });
}

render() {
  return (
    <View style={styles.container}>
      <ScrollView
        keyboardShouldPersistTaps='handled'
        contentContainerStyle={
          {height: Dimensions.get('window').height - 50}
        }>

        <View style={styles.container}>
          <Toolbar
            style={{
              container: {backgroundColor: '#ff7043'},
              centerElementContainer: {marginLeft: 0}
            }}
            leftElement={
              <TouchableHighlight
                onPress={this.onBackPressed}
                underlayColor="#ff7043">
                <Icon name="arrow-back" color="#fff"/>
              </TouchableHighlight>
            }

            centerElement={
              <Text style={ styles.logo }>REGISTRO</Text>
            }
          />

          <View style={styles.messageContainer}>
            <Text style={styles.welcomeText}>
              Información básica
            </Text>
            <Text />
            <Text style={styles.normalText}>
              Empecemos con tu información básica
              para crear tu cuenta.
            </Text>
          </View>

          <View style={styles.fieldsContainer}>
            <TextField
              ref={this.nameRef}
              keyboardType='default'
              autoCapitalize='none'
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitName}
              label='Nombre completo'
              returnKeyType='next'
              maxLength={150}
              baseColor='gray'
              tintColor='gray'
              textColor='gray'
              highlightColor='gray'/>

            <TextField
              ref={this.emailRef}
              keyboardType='email-address'
              autoCapitalize='none'
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitEmail}
              label='Correo electrónico'
              returnKeyType='next'
              maxLength={50}
              baseColor='gray'
              tintColor='gray'
              textColor='gray'
              highlightColor='gray'/>

            <TextField
              ref={this.PasswordRef}
              autoCapitalize='none'
              autoCorrect={false}
              enablesReturnKeyAutomatically={true}
              onFocus={this.onFocus}
              onChangeText={this.onChangeText}
              onSubmitEditing={this.onSubmitPassword}
              returnKeyType='done'
              label='Contraseña'
              maxLength={10}
              baseColor='gray'
              tintColor='gray'
              textColor='gray'
              highlightColor='gray'/>
          </View>

          <View style={styles.buttonContainer}>
            <Button
              raised
              primary
              onPress={this.onSubmitPassword}
              style={buttonStyle}
              text="Continuar" />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

}
