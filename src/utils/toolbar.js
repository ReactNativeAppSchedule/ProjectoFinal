import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import { 
  Icon,
  Toolbar,
} from 'react-native-material-ui';

import settings from '../config/conf.js';

const ToolbarD = props => {
  const {
    iconColorLeft,
    iconColorRight,
    onPressLeft,
    onPressRight
  } = props;

  return (
    <Toolbar
      style={{
        centerElementContainer: {marginLeft: 0}
      }}
      leftElement={
        <TouchableHighlight
          onPress={onPressLeft}
          underlayColor={settings.colorPrimary}>
          <Icon name="arrow-back" color={iconColorLeft}/>
        </TouchableHighlight>
      }
      centerElement={
        <Text style={{color: '#fff', fontSize: 13}}>MATERIAS</Text>
      }
      rightElement={
        <TouchableHighlight
          onPress={onPressRight}
          underlayColor={settings.colorPrimary}>
          <Icon name="done" color={iconColorRight}/>
        </TouchableHighlight>
      }
    />
  )
}

const styles = StyleSheet.create({
 logo: {
    width: 60,
    alignSelf: 'center',
  },
});

export default ToolbarD;