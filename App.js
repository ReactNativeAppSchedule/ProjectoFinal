import React, { Component } from 'react';
import {
  ThemeProvider,
} from 'react-native-material-ui';
import {
  ActionConst,
  Action,
  Router,
  Scene,
  Stack,
} from 'react-native-router-flux';

import LoginView from './src/login/login.component';
import Registro1View from './src/registro1/registro1.component';
import ListaMedicamentosView from './src/ListaMedicamentos/listaMedicamentos.component';
import ListaTareasView from './src/ListaTareas/listaTareas.component';
import CitaProgramadaView from './src/CitaProgramada/citaProgramada.component';
import AgregarTareaView from './src/AgregarTarea/agregarTarea.component';
import VerTareaView from './src/cronometro/cronometro.component';

export default class App extends Component<{}> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <Router>
          <Stack key="mainApp">
            <Scene
              key="loginView"
              component={LoginView}
              title="LoginView"
              hideNavBar/>
            <Scene
              key="registro1View"
              component={Registro1View}
              title="Registro1View"
              hideNavBar />
            <Scene
              key="listaMedicamentosView"
              component={ListaMedicamentosView}
              title="ListaMedicamentosView"
              hideNavBar />
            <Scene
              key="citaProgramadaView"
              component={CitaProgramadaView}
              title="CitaProgramadaView"
              hideNavBar />
            <Scene
              key="listaTareasView"
              component={ListaTareasView}
              title="ListaTareasView"
              hideNavBar />
            <Scene
              key="agregarTareaView"
              component={AgregarTareaView}
              title="AgregarTareaView"
              hideNavBar />
            <Scene
              key="verTareaView"
              component={VerTareaView}
              title="VerTareaView"
              hideNavBar />
          </Stack>
        </Router>
      </ThemeProvider>
    );
  }
}

const uiTheme = {
  palette: {
    primaryColor: '#ff7043',
  },
  toolbar: {
    container: {
      height: 35,
      elevation: 0
    },
  },
};
