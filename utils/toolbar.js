import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

import { 
  Icon,
  Toolbar,
} from 'react-native-material-ui';

import settings from '../config/conf.js';

const ToolbarD = props => {
  const {
    iconColorLeft,
    iconColorRight,
    onPressLeft,
    onPressRight
  } = props;

  return (
    <Toolbar
      style={{
        centerElementContainer: {marginLeft: 0}
      }}
      leftElement={
        <TouchableHighlight
          onPress={onPressLeft}
          underlayColor={settings.colorPrimary}>
          <Icon name="arrow-back" color={iconColorLeft}/>
        </TouchableHighlight>
      }
      centerElement={
          <Image
            source={settings.logoImage}
            style={ styles.logo }
            resizeMode='contain'/>
      }
    />
  )
}

const styles = StyleSheet.create({
 logo: {
    width: 60,
    alignSelf: 'center',
  },
});

export default ToolbarD;