import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  Dimensions,
  ActivityIndicator
} from 'react-native';

const Loader = props => {
  const {
    loading,
    text
  } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {console.log('close modal')}}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <View style={{flex: 1}}>
            <ActivityIndicator
              size={50}
              animating={loading}
              color="#03a9f4" />
            </View>
          <View style={{flex: 2, marginHorizontal: 10}}>
            <Text style={styles.instructions}>
              {text}
            </Text>
          </View>
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 150,
    width: Dimensions.get('window').width - 50,
    marginHorizontal: 50,
    borderRadius: 10,
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

export default Loader;
